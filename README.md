# ics-ans-role-jmasar

Ansible role to install [jmasar-service](https://gitlab.esss.lu.se/ics-software/jmasar-service).

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## Role Variables

```yaml
jmasar_version: 1.0.3
# profile should be test or production
jmasar_profile: test
jmasar_postgres_tag: 10
jmasar_postgres_user: jmasar
jmasar_postgres_password: secret
jmasar_postgres_db: jmasar
# Host interface to expose postgres port
# Hostnames are not allowed! Use 0.0.0.0 to allow access from other machines
jmasar_postgres_published_interface: 127.0.0.1
# server_name used in nginx config and jmasar.service (it can't be a list!)
jmasar_server_name: "{{ ansible_fqdn }}"
jmasar_epics_ca_addr_list: "localhost"
jmasar_epics_ca_auto_addr_list: "NO"
# Parameters for logging
jmasar_graylog_host: udp:127.0.0.1
jmasar_graylog_port: 12201
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-jmasar
```

## License

BSD 2-clause
