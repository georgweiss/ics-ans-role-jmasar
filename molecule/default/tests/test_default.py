import os
import pytest
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize("name", ["jmasar.service", "nginx"])
def test_service_enabled_and_running(host, name):
    service = host.service(name)
    assert service.is_enabled
    assert service.is_running


# The swagger-ui is only enabled for the test profile
def test_jmasar_swagger_ui(host):
    cmd = host.command(
        "curl -k -L http://localhost:8080/swagger-ui.html"
    )
    assert "<title>Swagger UI</title>" in cmd.stdout


def test_jmasar_selected_profile(host):
    with host.sudo():
        cmd = host.run("journalctl -u jmasar.service")
    assert "The following profiles are active: test" in cmd.stdout
